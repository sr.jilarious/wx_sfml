
#include <SFML/Graphics.hpp>
#include <wx/wx.h>

#include "wxSfmlCanvas.h"
#include "main.h"


TestFrame::TestFrame() :
    wxFrame(NULL, wxID_ANY, "SFML 2.5 w/ wxWidgets 3.1", wxDefaultPosition, wxSize(650, 490))
{
    mCanvas = new TestSfmlCanvas(this, wxID_ANY, wxPoint(5, 25), wxSize(640, 480));

    // Also add a button.
     wxButton *button = new wxButton(
            this, 
            wxID_ANY, 
            wxT("Toggle Size"),
            wxPoint(5, 5)
        );
     button->Bind(wxEVT_BUTTON, [&](wxCommandEvent& arg) -> void {
         mCanvas->toggleSize();
     });

    

    wxBoxSizer* mainSizer =  new wxBoxSizer( wxVERTICAL );
    
    mainSizer->Add(mCanvas, 6, wxALIGN_TOP | wxEXPAND);
    mainSizer->Add(button, 0, wxALIGN_RIGHT | wxALIGN_BOTTOM);
    SetSizerAndFit(mainSizer);
}

TestSfmlCanvas::TestSfmlCanvas(
        wxWindow*  Parent,
        wxWindowID Id,
        wxPoint&   Position,
        wxSize&    Size,
        long       Style
    ) : wxSfmlCanvas(Parent, Id, Position, Size, Style),
        mLarge(false)
{
    // Load a texture and create a sprite.
    mTexture.loadFromFile("data/ball.png");
    mSprite = std::make_unique<sf::Sprite>(mTexture);
}

void 
TestSfmlCanvas::OnUpdate()
{
    clear(sf::Color(64, 196, 196));
    draw(*mSprite);
}

void
TestSfmlCanvas::toggleSize()
{
    if (mLarge) {
        mSprite->setScale(sf::Vector2f(1.2f, 1.2f));
    } 
    else {
        mSprite->setScale(sf::Vector2f(0.5f, 0.5f));
    }

    mLarge = !mLarge;
}

IMPLEMENT_APP(TestApplication);
