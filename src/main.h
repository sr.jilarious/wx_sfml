#pragma once

#include <memory>

#include <SFML/Graphics.hpp>
#include <wx/wx.h>

#include "wxSfmlCanvas.h"

// Our overridden class that does some SFML drawing.
class TestSfmlCanvas : public wxSfmlCanvas
{
public:

    TestSfmlCanvas(
            wxWindow*  Parent,
            wxWindowID Id,
            wxPoint&   Position,
            wxSize&    Size,
            long       Style = 0
    );

    void toggleSize();

protected:
    void OnUpdate() override;

private:
    sf::Texture mTexture;
    std::unique_ptr<sf::Sprite> mSprite;
    bool mLarge;
};


// wx Frame to contain the main canvas control.  Can have extra controls added to it as desired.
class TestFrame : public wxFrame
{
public :
    TestFrame();

protected:
    TestSfmlCanvas* mCanvas;

};

// Main wx Application instance.
class TestApplication : public wxApp
{
private :

    virtual bool OnInit()
    {
        // Create the main window
        TestFrame* MainFrame = new TestFrame;
        MainFrame->Show();

        return true;
    }
};